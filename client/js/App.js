'use strict';
import $ from 'jquery';
import appStyles from '../sass/app.scss';

//-- Autocomplete component
$(function(){
    var endpoint = "http://localhost:3000",
        data = [];
    // Search suggestions ----------------------------------------------------------------------
    var search = {
        suggesstionBox: $(".suggesstion-box"),
        loader : $("#loader"),
        searchBox : $("input#search"),
        clearButton : $("#clear"),
        init:function(){
            this.getCards(); 
        },
        clear:function(){
            $("input").val("");
            this.clearButtonHide();
            this.suggesstionBox.slideUp();
        },
        loaderShow: function(){
            this.loader.show();
        },
        loaderHide: function(){
            this.loader.hide();
        },
        clearButtonShow(){
            this.clearButton.show();
        },
        clearButtonHide(){
            this.clearButton.fadeOut();
        },
        pressEnterURL: function(term){
            
        },
        populateBox: function(data,input){
            var input = input,
                elements = "",
                input = $(input).siblings(".suggesstion-box"),
                promised = this.promise(data, input);
            this.loaderShow();
            promised.done(function(){
                if( promised.status != "404" && promised.status != "503"){
                    let data = promised.responseJSON; 
                    if(data.count > 0){
                        $.each(data.data,function(){
                            elements += `<a href="#" data-abb="${this.abbreviation}">${this.name}</a>` 
                        })
                    }else if(data.count == 0){
                        elements += `<a>${data.message}</a>` 
                    }
                }else{
                    elements += `<a>${data.statusText}</a>` 
                }                
                this.loaderHide();
                this.clearButtonShow();
                input.html(elements).slideDown();
            }.bind(this))
        },
        promise:function(){
            return $.ajax({
                data : data,
                dataType: 'json',
                async: true,
                type : 'GET',
                url: `${endpoint}/api/states`
            });
        }
    }
    // Search events
    if($("#search").length > 0){
        var searchInput = search.searchBox;
        searchInput.keyup(function(e){
            var input = $(this);
            if (input.val().length > 2 || e.which == 13 ){
                data = {
                    "term": input.val(),
                }
                search.populateBox(data,input);
            }
            if (input.val().length == 0 && e.which == 8 ){
                search.clear();
            }


        }).on("blur", function(){
            search.suggesstionBox.slideUp();
        });
        
        search.clearButton.on('click',function(){
            search.clear();
        });
        $(document).on("click", ".suggesstion-box a", function(e){
            e.preventDefault();
            searchInput.val($(this).text());      
        })
        $('document').keypress(function(e) {
            if (e.which == 38) {
                //scroll up
            } else if (e.which == 40) {
                //scroll down
            }   
         });
    }//if
});